package com.charudet.lab10;

public class Triangle extends Shape{
	private double a;
	private double b;
	private double c;

	public Triangle(double a, double b, double c) {
		super("Triangle");
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public double getA() {
		return a;
	}

	public double getB() {
		return b;
	}

	public double getC() {
		return c;
	}
	@Override
	public String toString() {
		return this.getName() + " a : " + this.a + " b : " + this.b + " c : " + this.c;
	}

	@Override
	public double calArea() {
		double s = (int) ((a + b + c) / 2);
		double area = ((int) s * (s - a) * (s - b) * (s - c));
		return Math.sqrt(area);
	}

	@Override
	public double calPrimeter() {
		return a + b + c;
	}
}
